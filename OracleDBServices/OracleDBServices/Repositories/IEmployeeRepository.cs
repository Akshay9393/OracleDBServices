﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OracleDBServices.Repositories
{
    public interface IEmployeeRepository
    {
        object GetEmployeeList();

        object GetEmployeeDetails(int empId);
    }
}
